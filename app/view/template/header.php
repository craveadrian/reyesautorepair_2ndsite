<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"/> -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php if ($view == "home"): ?>
    <link rel="canonical" href="<?php echo URL; ?>" />
	<?php endif; ?>
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<div class="headerLeft">
					<a href="<?php echo URL; ?>"> <img src="public/images/common/mainLogo.png" class="header-logo" alt="<?php $this->info("company_name"); ?> Main logo"> </a>
				</div>
				<nav>
					<a href="#" id="pull"><strong>MENU</strong></a>
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>">HOME</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content">GALLERY</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content">SERVICES</a></li>
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content">CONTACT US</a></li>
					</ul>
				</nav>
				<div class="headerRight">
					<a href="tel:<?php $this->info("phone") ?>">
						<img src="public/images/common/header-phone.png" class="header-phone" alt="Header Phone">
					</a>
				</div>
			</div>
		</div>
	</header>

	<?php if($view == "home"):?>
		<div id="banner">
			<div class="slider">
				<img src="public/images/common/banner-img1.jpg" alt="Slider1">
			</div>
			<div class="row">
				<h3>LOCATED CONVENIENTLY OFF OF HIGHWAY 244</h3>
				<h2>SERVICE & REPAIR</h2>
				<h3>PROUDLY SERVING TULSA, OK & SURROUNDING AREAS!</h3>
				<div class="links">
					<p class="banner-phone">
						<?php $this->info(["phone","tel"]); ?>
						<?php $this->info(["phone2","tel"]); ?>
					</p>
					<a href="contact#content" class="btn">FREE DIAGNOSTIC</a>
				</div>
			</div>
		</div>
	<?php endif; ?>
