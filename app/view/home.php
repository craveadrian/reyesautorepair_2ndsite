<div id="services">
	<div class="row">
		<h2> <span class="blue">Our</span> <span class="orange">Services</span> </h2>
		<div class="flex-container">
			<dl>
				<dt>
					<div class="image">
						<img src="public/images/content/services-img1.jpg" alt="OIL CHANGES">
						<a href="services#content"><span>more</span></a>
					</div>
				</dt>
				<dd>
					OIL CHANGES
				</dd>
			</dl>
			<dl>
				<dt>
					<div class="image">
						<img src="public/images/content/services-img2.jpg" alt="TUNE-UPS">
						<a href="services#content"><span>more</span></a>
					</div>
				</dt>
				<dd>
					TUNE-UPS
				</dd>
			</dl>
			<dl>
				<dt>
					<div class="image">
						<img src="public/images/content/services-img3.jpg" alt="DIAGNOSTIC SCANS">
						<a href="services#content"><span>more</span></a>
					</div>
				</dt>
				<dd>
					DIAGNOSTIC SCANS
				</dd>
			</dl>
		</div>
	</div>
</div>
<div id="welcome">
	<div class="row">
		<div class="panel">
			<img src="public/images/common/car.png" alt="car">
			<div class="text">
				<h1> <span class="orange">Reyes</span> <span class="blue"> Auto Repair</span> </h1>
				<p> <strong>At Reyes Auto Repair, we tell it like it is and always keep our customers’ best interest in mind.</strong> </p>
				<p>We are establishing a reputation as an auto shop that‘s honest and consistently delivers great value. Our regular customers know we’ll always shoot straight with them when they bring in their vehicles for service, routine maintenance or emergency care. We have the expertise to work on all makes and models of cars and trucks, imports and domestics, including pickup trucks and work vans.</p>
				<a href="services#content" class="btn">READ MORE</a>
			</div>
		</div>
	</div>
</div>
<div id="phone">
	<div class="row">
		<p>
			<?php $this->info(["phone","tel"]); ?>
			<?php $this->info(["phone2","tel"]); ?>
		</p>
	</div>
</div>
<div id="why">
	<div class="row">
		<h2>Why Choose Us?</h2>
		<div class="flex-container">
			<dl>
				<dt><img src="public/images/content/why1.png" alt="EFFICIENT WORK"></dt>
				<dd>EFFICIENT WORK</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/why2.png" alt="QUICK"></dt>
				<dd>QUICK</dd>
			</dl>
			<dl>
				<dt><img src="public/images/content/why3.png" alt="REASONABLE PRICE"></dt>
				<dd>REASONABLE PRICE</dd>
			</dl>
		</div>
	</div>
</div>
<div id="recent">
	<div class="row">
		<h2>Recent Project</h2>
		<div class="flex-container">
			<div class="images">
				<div class="imgTop">
					<img src="public/images/content/recent1.png" alt="Recent Project 1">
					<img src="public/images/content/recent2.png" alt="Recent Project 2">
					<img src="public/images/content/recent3.png" alt="Recent Project 3">
				</div>
				<div class="imgBottom">
					<img src="public/images/content/recent4.png" alt="Recent Project 4">
					<img src="public/images/content/recent5.png" alt="Recent Project 5">
					<img src="public/images/content/recent6.png" alt="Recent Project 6">
				</div>
				<div class="view">
					<a href="#" class="vbtn">VIEW <br> GALLERY</a>
				</div>
			</div>
			<div class="socials">
				<p>
					<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">f</a>
					<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">l</a>
					<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">x</a>
					<a href="<?php $this->info("rs_link") ?>" class="socialico" target="_blank">r</a>
				</p>
			</div>
		</div>
	</div>
</div>
<div id="reviews">
	<div class="row">
		<img src="public/images/content/reviews1.png" alt="Review Image 1">
		<div class="container">
			<div class="text">
				<h2> <span class="blue">What</span>	<span class="orange">They Say</span> </h2>
				<p>The owner took the time to talk to me and explain to me what was wrong with my car. The techs were quick and knowledgeable. The office staff was very friendly, helpful and welcoming.</p>
				<p>- Ellie N.</p>
				<a href="reviews#content" class="btn">VIEW MORE</a>
			</div>
		</div>
	</div>
</div>
<div id="contacts">
	<div class="logo">
		<div class="row">
			<a href="<?php echo URL; ?>"><img src="public/images/common/botIcon.png" alt="<?php $this->info("company_name")?> Bottom Main Icon" class="botLogo"></a>
			<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
				<h2>Contact Us</h2>
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
				<label><span class="ctc-hide">Message</span>
					<textarea name="message" cols="30" rows="10" placeholder="Message/Questions:"></textarea>
				</label>
				<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
				<div class="bot">
					<label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label>
					<div class="g-recaptcha"></div>
					<label>
						<input type="checkbox" name="consent" class="consentBox"> <span>I hereby consent to having this website store my submitted information so that they can respond to my inquiry.</span>
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/> <span> I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a></span>
					</label>
					<?php endif ?>
				</div>
			</form>
		</div>
	</div>
	<div class="info">
		<div class="row">
			<p class="phone"><?php $this->info(["phone","tel"]);?>/<?php $this->info(["phone2","tel"]);?></p>
			<p class="email"><?php $this->info(["email","mail"]);?></p>
			<p class="address"><?php $this->info("address");?></p>
			<p class="social-media">
				<a href="<?php $this->info("fb_link") ?>" class="socialico" target="_blank">f</a>
				<a href="<?php $this->info("tt_link") ?>" class="socialico" target="_blank">l</a>
				<a href="<?php $this->info("yt_link") ?>" class="socialico" target="_blank">x</a>
				<a href="<?php $this->info("rs_link") ?>" class="socialico" target="_blank">r</a>
			</p>
		</div>
	</div>
</div>
<div id="map">
	<a href="https://www.google.com/maps/place/702+N+Sheridan+Rd,+Tulsa,+OK+74115,+USA/@36.1651114,-95.9071192,17z/data=!3m1!4b1!4m5!3m4!1s0x87b6ed83c2114d8b:0x3adf10c9a7072744!8m2!3d36.1651114!4d-95.9049305" target="_blank">
		<img src="public/images/common/map.jpg" alt="Map" class="Gmap">
	</a>
</div>
